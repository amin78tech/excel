<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('drugs', function (Blueprint $table) {
            $table->id();
            $table->longText('drug_name')->nullable();
            $table->longText('slot')->nullable();
            $table->longText('dosage_Form')->nullable();
            $table->longText('strengh')->nullable();
            $table->longText('route_of_Admin')->nullable();
            $table->longText('atc_code')->nullable();
            $table->longText('ingredient')->nullable();
            $table->longText('approved_clinical_indications')->nullable();
            $table->longText('access_level')->nullable();
            $table->longText('remarks')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('drugs');
    }
};
