<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DrugSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $path = file_get_contents(database_path('json/orginal-file-two.json'));
        $arr = json_decode($path,true);
        foreach ($arr as $item){
            $data[] = $item;
        }
        foreach ($data as $item){
            \App\Models\Drug::factory($item)->create();
        }

    }
}
