<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//Route::get('/', function () {
//    $id_drug_final = \App\Models\DrugSeeder::query()->where('dosage_form', 'GEL')->pluck('id')->toArray();
//    dd($id_drug_final);
//    $id_dosage_forms = 18;
//    foreach ($id_drug_final as $item){
//        \App\Models\DosageForm_DrugFinal::query()->insert(['drug_final_id' => $item, 'dosage_form_id' => $id_dosage_forms,
//        ]);
//    };
//    return 'ok';
//});
Route::get('/',function (){
//    $file = file_get_contents(database_path('json/orginal-file-two.json'));
//    $data =json_decode($file ,true);
//    foreach ($data as $key=>$item){
//        $res = explode(',', $item['dosage_Form']);
//        foreach ($res as $itm){
//            $final[] = rtrim(ltrim($itm));
//        }
//    };
//    foreach (array_values(array_unique($final)) as $it) {
//        \App\Models\Dosage::query()->insert(['name' => $it,]);
//    }



    $dosage = \App\Models\Dosage::query()->get()->toArray();
    $drugs = \App\Models\Drug::query()->get()->toArray();
        foreach ($drugs as $itm){
            $exp = explode(',', $itm['dosage_Form']);
            foreach ($dosage as $item) {
                foreach ($exp as $i) {
                    if (rtrim(ltrim($i)) == $item['name']) {
                        \App\Models\DrugDosage::query()->insert(['drug_id' => $itm['id'], 'dosage_id' => $item['id']]);
                    }
                }
            }
        }

});
